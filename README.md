# Random Character Generator

This project generates random & unique characters (glyphs) using OpenGL, GLEW & SDL2.
All external libraries are the property of their respective owners.

## Can I Use This?

Unless explicitly given permission, No.

## Dependencies
1. OpenGL
2. GLEW
3. SDL2
4. C Mathematics Library

## Compilation

`make all`