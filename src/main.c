#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>

/* Types */
typedef enum cg1_et {
    // none
    cg1_et_no_error,

    // file loading
    cg1_et_unable_to_open_file,
    cg1_et_file_larger_than_2GB,

    // random file loading
    cg1_et_unable_to_open_dev_urandom,

    // graphics initialization
    cg1_et_sdl2_initialization_failure,
    cg1_et_sdl2_window_opening_failure,
    cg1_et_sdl2_context_creation_failure,
    cg1_et_glew_initialization_failure,

    // shader compilation
    cg1_et_invalid_vertex_shader_source,
    cg1_et_invalid_fragment_shader_source,
    cg1_et_vertex_compilation_failed,
    cg1_et_fragment_compilation_failed,
    cg1_et_shader_linking_failed,

    // unknown
    cg1_et_unknown_error
} cg1_et;

typedef struct cg1_bytes {
    unsigned long long p_length;
    unsigned char* p_data;
} cg1_bytes;

typedef struct cg1_random {
    unsigned long long p_seed;
    cg1_bytes p_random_data;
} cg1_random;

typedef struct cg1_shaders {
    GLuint p_program_ID;
    char* p_vertex_file_address;
    char* p_fragment_file_address;
    char* p_vertex_file;
    char* p_fragment_file;
    GLuint p_vertex_ID;
    GLuint p_fragment_ID;
} cg1_shaders;

typedef struct cg1_graphics {
    SDL_Window* p_window;
    SDL_GLContext* p_context;
    unsigned char p_sdl2_initialization;
} cg1_graphics;

typedef struct cg1_stroke_node {
    float p_x;
    float p_y;
    float p_width;
    float p_rotation;
} cg1_stroke_node;

typedef struct cg1_stroke {
    cg1_stroke_node p_start;
    cg1_stroke_node p_end;
} cg1_stroke;

typedef struct cg1_character {
    cg1_stroke_node* p_stroke_nodes;
    unsigned long long p_stroke_node_count;
    unsigned long long p_vbo_length;
    unsigned long long p_ebo_length;
    float* p_vbo_data;
    unsigned int* p_ebo_data;
    GLuint p_vbo;
    GLuint p_ebo;
    GLuint p_vao;
} cg1_character;

/* Library Functions */
char* cg1_string_copy(char* input) {
    char* output;
    unsigned long long length = 0;

    while (input[length] != 0) {
        length++;
    }

    output = (char*)malloc(sizeof(char) * length);

    for (unsigned long long i = 0; i < length; i++) {
        output[i] = input[i];
    }

    return output;
}

char* cg1_load_file(cg1_et* error, char* file_path) {
    char* output = 0;
    long long length = 0;
    FILE* f;

    // open the file
    f = fopen(file_path, "rb");

    // get file length
    if (f)
    {
        fseek(f, 0, SEEK_END);
        length = ftell(f);
        rewind(f);
    }
    else
    {
        *error = cg1_et_unable_to_open_file;
        return 0;
    }

    // check if file is too large
    if (length > 2147483647) // subtracted 1 for null termination
    {
        fclose(f);
        
        *error = cg1_et_file_larger_than_2GB;

        return 0;
    }

    // create output
    output = (char*)malloc(sizeof(char) * (length + 1));

    // read file into memory
    length = fread(output, 1, length, f);

    // close file
    fclose(f);

    // write the null termination
    output[length] = 0;

    // return the binary buffer
    return output;
}

cg1_bytes cg1_generate_random_bytes(cg1_et* error, unsigned long long length) {
    cg1_bytes output;
    FILE* f;

    output.p_length = length;
    output.p_data = (unsigned char*)malloc(sizeof(unsigned char) * length);

    f = fopen("/dev/urandom", "rb");
    if (f) {
        fread(output.p_data, 1, length, f);

        fclose(f);
    } else {
        *error = cg1_et_unable_to_open_dev_urandom;

        return output;
    }

    return output;
}

void cg1_destroy_bytes(cg1_bytes* bytes) {
    cg1_bytes b = *bytes;

    free(b.p_data);

    return;
}

/* Error Handling */
void cg1_print_errors(cg1_et main_error, cg1_et sub_error) {
    printf("Error -> Main Error Code: %i; Sub Error Code: %i;\n", main_error, sub_error);

    return;
}

/* Random */
cg1_random* cg1_generate_random(cg1_et* error, unsigned long long seed, unsigned long long length) {
    cg1_random* output = (cg1_random*)malloc(sizeof(cg1_random));

    output->p_seed = seed;
    output->p_random_data = cg1_generate_random_bytes(error, length);

    return output;
}

void cg1_destroy_random(cg1_random** random) {
    cg1_random* r = *random;

    cg1_destroy_bytes(&(r->p_random_data));

    free(*random);

    *random = 0;

    return;
}

/* Shaders */
void cg1_destroy_shaders(cg1_shaders** shaders) {
    cg1_shaders* s = *shaders;

    // free gpu resources
    glDeleteShader(s->p_vertex_ID);
    glDeleteShader(s->p_fragment_ID);
    glDeleteProgram(s->p_program_ID);

    // free cpu resources
    free(s->p_vertex_file_address);
    free(s->p_fragment_file_address);

    if (s->p_vertex_file != 0) {
        free(s->p_vertex_file);
    }
    if (s->p_fragment_file != 0) {
        free(s->p_fragment_file);
    }

    // free shader variable
    free(*shaders);

    // set source variable to zero
    *shaders = 0;

    return;
}

cg1_shaders* cg1_create_shaders(cg1_et* main_error, cg1_et* sub_error, char* vertex_file_address, char* fragment_file_address) {
    cg1_shaders* output = (cg1_shaders*)malloc(sizeof(cg1_shaders));
    GLint success;

    // initialize values
    output->p_program_ID = glCreateProgram();
    output->p_vertex_file_address = cg1_string_copy(vertex_file_address);
    output->p_fragment_file_address = cg1_string_copy(fragment_file_address);
    output->p_vertex_ID = glCreateShader(GL_VERTEX_SHADER);
    output->p_fragment_ID = glCreateShader(GL_FRAGMENT_SHADER);

    // open files
    output->p_vertex_file = cg1_load_file(sub_error, vertex_file_address);
    if (*sub_error != cg1_et_no_error) {
        *main_error = cg1_et_invalid_vertex_shader_source;

        cg1_destroy_shaders(&output);

        return 0;
    }

    output->p_fragment_file = cg1_load_file(sub_error, fragment_file_address);
    if (*sub_error != cg1_et_no_error) {
        *main_error = cg1_et_invalid_vertex_shader_source;

        cg1_destroy_shaders(&output);

        return 0;
    }

    // send programs to gpu
    glShaderSource(output->p_vertex_ID, 1, (const GLchar* const*)&(output->p_vertex_file), NULL);
    glShaderSource(output->p_fragment_ID, 1, (const GLchar* const*)&(output->p_fragment_file), NULL);

    // compile shaders
    glCompileShader(output->p_vertex_ID);
    glCompileShader(output->p_fragment_ID);

    // check for compilation errors
    glGetShaderiv(output->p_vertex_ID, GL_COMPILE_STATUS, &success);
    if (!success) {
        *main_error = cg1_et_vertex_compilation_failed;

        cg1_destroy_shaders(&output);

        return 0;
    }

    glGetShaderiv(output->p_fragment_ID, GL_COMPILE_STATUS, &success);
    if (!success) {
        *main_error = cg1_et_vertex_compilation_failed;
        
        cg1_destroy_shaders(&output);

        return 0;
    }

    // link shaders together
    glAttachShader(output->p_program_ID, output->p_vertex_ID);
    glAttachShader(output->p_program_ID, output->p_fragment_ID);

    glLinkProgram(output->p_program_ID);
    
    glGetProgramiv(output->p_program_ID, GL_LINK_STATUS, &success);
    if (!success) {
        *main_error = cg1_et_shader_linking_failed;

        cg1_destroy_shaders(&output);

        return 0;
    }

    // finished
    return output;
}

void cg1_use_shaders(cg1_shaders* shaders) {
    glUseProgram(shaders->p_program_ID);

    return;
}

/* Graphics Library Handling */
void cg1_destroy_graphics(cg1_graphics** graphics) {
    cg1_graphics* g = *graphics;

    // clean up window
    if (g->p_context != 0) {
        SDL_GL_DeleteContext(g->p_context);
    }
    if (g->p_window != 0) {
        SDL_DestroyWindow(g->p_window);
    }
    if (g->p_sdl2_initialization == 0) {
        SDL_Quit();
    }

    // free variable
    free(*graphics);

    // invalidate original variable
    *graphics = 0;

    return;
}

cg1_graphics* cg1_initalize_graphics(cg1_et* error, char* window_title, int window_width, int window_height) {
    cg1_graphics* output = (cg1_graphics*)malloc(sizeof(cg1_graphics));
    
    // initialize SDL2
    output->p_sdl2_initialization = 0;
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        *error = cg1_et_sdl2_initialization_failure;
        output->p_sdl2_initialization = 1;

        cg1_destroy_graphics(&output);

        return 0;
    }

    // create window
    output->p_window = SDL_CreateWindow(window_title, 0, 0, window_width, window_height, SDL_WINDOW_OPENGL);
    if (output->p_window == 0) {
        *error = cg1_et_sdl2_window_opening_failure;

        cg1_destroy_graphics(&output);

        return 0;
    }

    // create opengl context
    output->p_context = SDL_GL_CreateContext(output->p_window);
    if (output->p_context == 0) {
        *error = cg1_et_sdl2_context_creation_failure;

        cg1_destroy_graphics(&output);

        return 0;
    }

    // initialize glew
    if (glewInit() != GLEW_OK) {
        *error = cg1_et_glew_initialization_failure;

        cg1_destroy_graphics(&output);

        return 0;
    }

    // graphics are up and running
    return output;
}

/* Events */
unsigned char should_quit() {
    SDL_Event e;
    SDL_Event* ep = &e;
    GLenum error;
    
    // get events
    while (SDL_PollEvent(ep)) {
        if (e.type == SDL_QUIT) {
            return 1;
        }
    }

    // get opengl errors
    error = glGetError();
    if (error != GL_NO_ERROR) {
        printf("Opengl Error Code: %i\n", error);
        return 1;
    }

    return 0;
}

/* Characters */
void cg1_bind_character(cg1_character* character) {
    glBindVertexArray(character->p_vao);
    glBindBuffer(GL_ARRAY_BUFFER, character->p_vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, character->p_ebo);

    return;
}

void cg1_unbind_character() {
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    return;
}

cg1_character* cg1_initialize_character(cg1_stroke_node* stroke_nodes, unsigned long long stroke_node_count, float scale_x, float scale_y) {
    // create variables
    cg1_character* output;
    unsigned long long vbo_index = 0;
    unsigned long long ebo_index = 0;
    
    // setup allocations
    output = (cg1_character*)malloc(sizeof(cg1_character));
    output->p_vbo_data = (float*)malloc(sizeof(float) * ((8 * (stroke_node_count - 1)) + 4));
    output->p_ebo_data = (unsigned int*)malloc(sizeof(unsigned int) * 6 * stroke_node_count);

    // create vertices
    for (unsigned long long i = 0; i < stroke_node_count - 1; i++) {
        // top left
        output->p_vbo_data[vbo_index + 0] = stroke_nodes[i].p_x * scale_x;
        output->p_vbo_data[vbo_index + 1] = stroke_nodes[i].p_y * scale_y;
    
        // top right
        output->p_vbo_data[vbo_index + 2] = (stroke_nodes[i].p_x + cos(stroke_nodes[i].p_rotation) * stroke_nodes[i].p_width) * scale_x;
        output->p_vbo_data[vbo_index + 3] = (stroke_nodes[i].p_y + sin(stroke_nodes[i].p_rotation) * stroke_nodes[i].p_width) * scale_y;

        // bottom left
        output->p_vbo_data[vbo_index + 4] = stroke_nodes[i + 1].p_x * scale_x;
        output->p_vbo_data[vbo_index + 5] = stroke_nodes[i + 1].p_y * scale_y;
    
        // bottom right
        output->p_vbo_data[vbo_index + 6] = (stroke_nodes[i + 1].p_x + cos(stroke_nodes[i + 1].p_rotation) * stroke_nodes[i + 1].p_width) * scale_x;
        output->p_vbo_data[vbo_index + 7] = (stroke_nodes[i + 1].p_y + sin(stroke_nodes[i + 1].p_rotation) * stroke_nodes[i + 1].p_width) * scale_y;

        // elements
        output->p_ebo_data[ebo_index + 0] = (i * 4) + 0;
        output->p_ebo_data[ebo_index + 1] = (i * 4) + 1;
        output->p_ebo_data[ebo_index + 2] = (i * 4) + 2;
        output->p_ebo_data[ebo_index + 3] = (i * 4) + 1;
        output->p_ebo_data[ebo_index + 4] = (i * 4) + 2;
        output->p_ebo_data[ebo_index + 5] = (i * 4) + 3;

        // increment variables
        vbo_index += 8;
        ebo_index += 6;
    }

    // create buffers on gpu
    glGenVertexArrays(1, &(output->p_vao));
    glGenBuffers(1, &(output->p_vbo));
    glGenBuffers(1, &(output->p_ebo));

    // bind for initialization
    cg1_bind_character(output);

    // initialize remaining output data
    output->p_vbo_length = vbo_index;
    output->p_ebo_length = ebo_index;
    output->p_stroke_nodes = stroke_nodes;
    output->p_stroke_node_count = stroke_node_count;

    // initialize gpu buffers
    glBufferData(GL_ARRAY_BUFFER, output->p_vbo_length * sizeof(float), output->p_vbo_data, GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, output->p_ebo_length * sizeof(unsigned int), output->p_ebo_data, GL_STATIC_DRAW);

    // setup vertex buffer layout
    // positions
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    // unbind
    cg1_unbind_character();

    // initialization complete
    return output;
}

void cg1_draw_character(cg1_character* stroke) {
    glDrawElements(GL_TRIANGLES, stroke->p_ebo_length, GL_UNSIGNED_INT, 0);

    return;
}

void cg1_destroy_character(cg1_character** character) {
    cg1_character* c = *character;

    glDeleteBuffers(1, &(c->p_ebo));
    glDeleteBuffers(1, &(c->p_vbo));
    glDeleteVertexArrays(1, &(c->p_vao));

    free(c->p_stroke_nodes);

    if (c->p_vbo_data != 0) {
        free(c->p_vbo_data);
    }
    if (c->p_ebo_data != 0) {
        free(c->p_ebo_data);
    }

    free(c);

    *character = 0;

    return;
}

cg1_stroke_node* cg1_generate_piece(cg1_stroke_node start, float end_thickness, float start_angle, float turn_angle, float common_connection_length, unsigned long long connection_count) {
    cg1_stroke_node* output = (cg1_stroke_node*)malloc(sizeof(cg1_stroke_node) * (connection_count + 1));
    cg1_stroke_node* previous = &(output[0]);
    cg1_stroke_node* temp_pointer = &(output[1]);

    // create the first stroke node
    memcpy(previous, &start, sizeof(cg1_stroke_node));

    // create the middle stroke nodes
    for (unsigned long long i = 1; i < connection_count; i++) {
        // create the next stroke node
        memcpy(temp_pointer, previous, sizeof(cg1_stroke_node));

        // adjust the current node
        temp_pointer->p_x = previous->p_x + (float)sin(turn_angle * (float)i);
        temp_pointer->p_y = previous->p_y + (float)cos(turn_angle * (float)i);
        temp_pointer->p_width = previous->p_width - (end_thickness / 1.0f);
        temp_pointer->p_rotation = turn_angle * (float)i;

        previous = temp_pointer;
        temp_pointer = &(output[i + 1]);
    }

    return output;
}

cg1_character* cg1_generate_random_radical(cg1_random* random, unsigned long long piece_count, unsigned long long max_piece_stroke_count) {
    cg1_character* output = (cg1_character*)malloc(sizeof(cg1_character));
    cg1_stroke_node start;
    unsigned long long node_count = 50;

    start.p_x = 0.5f;
    start.p_y = 0.5f;
    start.p_width = 0.5f;
    start.p_rotation = 0.0f;

    return cg1_initialize_character(cg1_generate_piece(start, 0.01f, 0.0f, 0.2f, 0.01, node_count), node_count, 0.1f, 0.1f);
}

int main() {
    cg1_et main_error = cg1_et_no_error;
    cg1_et sub_error = cg1_et_no_error;
    cg1_graphics* graphics;
    cg1_shaders* shaders;
    cg1_character* character;
    cg1_random* random;

    printf("Generating Random Character...\n");

    // initialize random data
    unsigned long long random_length = 1000;
    random = cg1_generate_random(&main_error, 123456, random_length);
    if (main_error != cg1_et_no_error) {
        goto cg1_label_destroy_random;
    }

    // initialize opengl
    graphics = cg1_initalize_graphics(&main_error, "Character Generator!", 720, 480);
    if (main_error != cg1_et_no_error) {
        goto cg1_label_destroy_graphics;
    }
    
    shaders = cg1_create_shaders(&main_error, &sub_error, "./src/shaders/v1/vertex.glsl", "./src/shaders/v1/fragment.glsl");
    if (main_error != cg1_et_no_error || sub_error != cg1_et_no_error) {
        goto cg1_label_destroy_shaders;
    }

    // use shaders
    cg1_use_shaders(shaders);

    // initialize gpu buffers
    character = cg1_generate_random_radical(random, 4, 3);

    // setup opengl drawing states
    glEnable(GL_DEPTH_TEST);
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    while (!should_quit()) {
        // clear the screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // do character drawing
        cg1_bind_character(character);
        cg1_draw_character(character);
        cg1_unbind_character();

        // update screen
        SDL_GL_SwapWindow(graphics->p_window);
    }

    cg1_label_destroy_character: cg1_destroy_character(&character);
    cg1_label_destroy_shaders: cg1_destroy_shaders(&shaders);
    cg1_label_destroy_graphics: cg1_destroy_graphics(&graphics);
    cg1_label_destroy_random: cg1_destroy_random(&random);

    if (main_error != cg1_et_no_error || sub_error != cg1_et_no_error) {
        cg1_print_errors(main_error, sub_error);
    }

    return 0;
}