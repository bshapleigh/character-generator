# The Character Structure

## What is the structure of a character?

Characters are made up of:

1. Radicals

Radicals are made up of:

1. Radicals
2. Pieces

Pieces are made up of:

1. Strokes

Strokes are made up of:

1. 2 Coords
2. 2 Rotations
3. 2 Widths
4. 1 Connection

## What are the rules for pieces?

1. They must be made up of strokes connected back to back.
2. Connected strokes do not have multiple in or out strokes per stroke.

## What are the rules for radicals?

1. They can be made up of one or more pieces & other radicals.
2. Those pieces & radicals can have one or more relations between other pieces & radicals.
